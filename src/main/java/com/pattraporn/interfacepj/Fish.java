/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.interfacepj;

/**
 *
 * @author Pattrapon N
 */
public class Fish extends AquaticAnimal implements Swimable{
    private String NameFish;
    
    public Fish(String NameFish) {
        super("Fish");
        this.NameFish=NameFish;
    }

    @Override
    public void swim() {
        System.out.println("Fish: "+NameFish+" swimming");
    }

    @Override
    public void eat() {
        System.out.println("Fish: "+NameFish+" eat food for fish");
    }


    @Override
    public void speak() {
        System.out.println("Fish: "+NameFish+" nomomo");
    }

    @Override
    public void sleep() {
        System.out.println("Fish: "+NameFish+" Zzzzzzz");
    }   
}
