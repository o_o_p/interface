/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.interfacepj;

/**
 *
 * @author Pattrapon N
 */
public class Crocodile extends Reptile implements Crawlable{
   private String NameCrocodile;
    
    public Crocodile(String NameCrocodile) {
        super("Crocodile", 4);
        this.NameCrocodile=NameCrocodile;
    }

    @Override
    public void crawl() {
        System.out.println("Crocodile: "+NameCrocodile+" crawl");
    }

    @Override
    public void eat() {
        System.out.println("Crocodile: "+NameCrocodile+" eat fish");
    }

  

    @Override
    public void speak() {
        System.out.println("Crocodile: "+NameCrocodile+" grrrrr");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile: "+NameCrocodile+" Zzzzzzz");
    } 
}
