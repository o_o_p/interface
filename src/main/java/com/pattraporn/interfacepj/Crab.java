/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.interfacepj;

/**
 *
 * @author Pattrapon N
 */
public class Crab extends AquaticAnimal implements Swimable{
    private String NameCrab;
    
    public Crab(String NameCrab) {
        super("Crab");
        this.NameCrab=NameCrab;
    }

    @Override
    public void swim() {
        System.out.println("Crab: "+NameCrab+" swimming");
    }

    @Override
    public void eat() {
        System.out.println("Crab: "+NameCrab+" eat food for Crab");
    }

  

    @Override
    public void speak() {
        System.out.println("Crab: "+NameCrab+" i have no voice!");
    }

    @Override
    public void sleep() {
        System.out.println("Crab: "+NameCrab+" Zzzzzzz");
    }   
}
