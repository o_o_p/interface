/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.interfacepj;

/**
 *
 * @author Pattrapon N
 */
public class Plane extends Vahicle implements Flyable,Runable{

    public Plane(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Plane: Start engine");
        
    }

    @Override
    public void stopEngine() {
       System.out.println("Plane:stopEngine");
    }

    @Override
    public void ralseSpeed() {
        System.out.println("Plane:Speed ​​up to 120 kilometers per hour");
    }

    @Override
    public void applyBreak() {
        System.out.println("Plane:Breakr");
    }

    @Override
    public void fly() {
        System.out.println("Plane:fly into the sky");
    }

    @Override
    public void run() {
        System.out.println("Plane:run");
        }
    
}
